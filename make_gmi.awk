#!/usr/bin/awk -f

# This script takes the output from html2text, assuming there's a Title: line,
# and writes the main body of the text to a file with name formed from the
# title.

# The title is expected to be in the first line fo the file, and any blank
# lines between the title and the body of the text will be skipped.

BEGIN {
    in_content = 0
    p_idx = 0
}

END {
    if(length(URL) > 10)
        print "\n## Generated from source URL:" >filename
        print "=> " URL >filename
}

# Match title heading and form a filename from it.

/^Title: / {
    sub(/^Title: /,"");
    filename = $0
    gsub(/[^A-Za-z0-9-]/,"_", filename)
    gsub(/_+/, "_", filename)
    filename = filename ".gmi"
    print filename
    next
}

# Any big HR lines, then truncate.
/^[=]{80}/{
    print substr($0, 1, 80) >filename
    next
}

# Any lines with 8 leading spaces are preformatted
/^[ ]{32}/ {
    if(p_idx == 0) {
        print "```" >filename
        p_idx = 1
    }
    print substr($0, 33) >filename
    next
}

/^[^ ]/ {
    if($0 != "" && p_idx == 1) {
        print "```" >filename
        p_idx = 0
    }
}

# Match the links that are listed in the references section

/^ +[0-9]++\. https?:\/\// {
    split($0, arr, " ")
    link_num = arr[1]
    gsub(/[^0-9]/, "", link_num)
    link_text = links[link_num]
    if(length(link_text) >= 5)
        print "=> " arr[2] " Link: " arr[1] " " link_text >filename
    else
        print "=> " arr[2] " Link: " arr[1] >filename

    next
}

# Match link-references in the body of the text, and sore preceding text.

/\[[0-9]+\]/ {
    array_size = split($0, arr, " ")
    for(i in arr) {
        idx = match(arr[i], /(.+)\[([0-9]+)\]/, parts)
        if(idx > 0) {
            links[parts[2]] = parts[1]
        }
    }
}

# The default handling writes lines  to our named file.

{
    # Skip any leading blank lines
    if(in_content == 0 && $0 == "")
        next;
    else
        in_content = 1

    print >filename
}

