#!/bin/bash

if [[ "${1}" == "" ]]; then
    echo "Usage $0: URL <html|text>"
    exit
fi

SCRIPT_DIR=$(dirname $(readlink -f $0))
H2T_RC="${SCRIPT_DIR}/html2textrc"
TMPFILE="/tmp/raw_${PPID}"

# Check if we're being asked for the HTML or plain-text intermediates...
if [[ "${2}" == "html" ]]; then
    readable --properties title,html-title,html-content -l force ${1} >${TMPFILE}.html
    echo "File in ${TMPFILE}.html"
    exit
elif [[ "${2}" == "text" ]]; then
    readable --properties title,html-title,html-content -l force ${1} | \
        html2text -links -nobs -from_encoding utf8 -width 4000 -rcfile ${H2T_RC} >${TMPFILE}.txt
    echo "File in ${TMPFILE}.txt"
    exit
fi

# At this point, we want to fully process the URL to Gemini markup
readable --properties title,html-title,html-content -l force ${1} | \
    html2text -links -nobs -from_encoding utf8 -width 4000 -rcfile ${H2T_RC} | \
    ${SCRIPT_DIR}/make_gmi.awk -v URL="${1}"

