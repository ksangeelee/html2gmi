# Web pages to Gemini with Reader Mode utility

TL;DR: This project allows you to specify the URL of an HTTP(S) page and
convert it to Gemini markup. It makes use of `readability-cli`, a project that
brings Firefox Reader Mode to NodeJS, and html2text (patched), and provides an
Bash script & Awk script to pull these bits together.

The rest of this document describes what the scripts do - you can mostly just
read the code to see how it works, there's not much to it.

```
kev@tp:~/html2gmi$ ./html2gmi.sh https://www.example.com/wordpres/gd32vf/
Retrieving...
Processing...
Longan_Nano_GD32VF103_SusaNET.gmi
```

The last line written shows the filename that was chosen to save the Gemini
markup, derived from the HTML document title. We can copy this file directly to
the Gemini server's content directory.

## More detail...

It can be useful to make some web pages available in Gemini space, but HTML is
surprisingly difficult to convert to plain text. A lot of heuristics are
involved, and they change along with web fashions.

Firefox does a great job of this in Reader mode, and the JavaScript code is
available to use. The GitLab user @gardenappl has taken this code and made it
into a CLI command to run in NodeJS. The project page is linked below and
includes instructions on how to install via npm.

=> https://gitlab.com/gardenappl/readability-cli

This provides a command 'readable' that can be used to process complex web
pages into much simplified HTML that can be converted into text using a utility
like 'html2text'.

Note that Firefox doesn't always offer Reader Mode; it sometimes decides it
can't reliably extract the text, in which case it simply doesn't offer the
Reader Mode option. By default, the 'readable' command follows exactly the same
rules (because it's using code taken straight from the Firefox codebase).

However, you can force 'readable' to always extract text by passing the
parameter --low-confidence (or just -l) with the value 'force'. See the example
below, and 'readable --help' for more details.

## html2text can create Gemini markup

Using html2text on this simplified markup generates good results. Using the
'-width' parameter with some huge number means that a paragraph will remain on
a single line (e.g. html2text -width 4000), which is ideal for gemtext.

Using the latest GitHub version (https://github.com/grobian/html2text) produces
a nice 'References' links list at the end of the page. It configured and
compiled easily on my Debian 10 system (just ./configure; make; sudo make
install).

The configuration file (e.g. ~/.html2textrc) can be used to further improve
gemtext compatible output during conversion. For example, the following
configuration defines suitable header tag equivalents, and specifies that one
blank line should space paragraphs vertically. Note that the baskslash (\)
character below is escaping a <SPACE> character in the config file.

```
H1.prefix = #\
H1.suffix =
H2.prefix = ##\
H2.suffix =
H3.prefix = ###\
H3.suffix =
H4.prefix = ###\
H4.suffix =
H5.prefix = ###\
H5.suffix =

P.vspace.after = 1
```

## Putting it together

This could all be done in a single pipe, but we'll write temp files to allow
viewing of the intermediates.

```
# Fetch the page to a file, force processing even when low-confidence.
readable -l force "https://www.bbc.co.uk/news/business-53337705" >/tmp/readble_test.html

# Convert to text with super-wide line width. 
html2text -links -from_encoding utf8 -width 5000 /tmp/readble_test.html >/tmp/readable_test.gmi

# For old version:  html2text -utf8 -width 5000 /tmp/readble_test.html >/tmp/readable_test.gmi

# We can check the file here, or post-process for further cleanup.
cat /tmp/readable_test.gmi
```

It might be possible to configure html2text to generate even more specific
output, see 'man 5 html2textrc' for all configuration options. However, at this
point it would be easy enough to just grep or sed the file to remove any lines
that are not wanted (e.g. lines with image tag alt-text).

Here's an example of text that was extracted from BBC News, with no extra
post-processing. This page was deemed low-confidence by Firefox, meaning it
didn't think it would convert well enough. I disagree, it looks more than good
enough!

=> /readable_test2.gmi An example page taken from the BBC News website, converted to text.

## Patching html2text to reference all images

By default, html2text seems to use the image Alt-Text in place of a link, I
presume because it's considered a valid alternative to the image. I'd prefer
all images to have links in the references, because it's often important to the
document content.

Therefore, I patched the Yacc/Bison code of html2text to output all images as
reference links, converting the alt-text to something that can easily be parsed
out of the document text to use with the link. It's a little hacky, but it
seems to work well enough. The patch is shown below: -

```diff
diff --git a/HTMLParser.yy b/HTMLParser.yy
index b51b781..27c742e 100644
--- a/HTMLParser.yy
+++ b/HTMLParser.yy
@@ -819,9 +819,18 @@ special:
        istr src = get_attribute(attr.get(), "SRC", "");
        istr alt = get_attribute(attr.get(), "ALT", "");
        /* when ALT is empty, and we have SRC, replace it with a link */
-       if (drv.enable_links && !src.empty() && alt.empty()) {
+       if (drv.enable_links && !src.empty() /* && alt.empty() */) {
                PCData *d = new PCData;
-               string nothing = "";
+               string nothing = "Image";
+        if(!alt.empty()) {
+            istr alt2 = alt.slice(0, 40);
+            alt2 += "_(Image)";
+            int max = alt2.length();
+            for(int idx=0; idx < max; idx++)
+                if(alt2.get(idx) == ' ')
+                    alt2.replace(idx, 1, '_');
+            d->text = alt2;
+        } else
             d->text = nothing;
         list<auto_ptr<Element>> *data = new list<auto_ptr<Element>>;
         data->push_back(auto_ptr<Element>(d));
```

After patching the code, you need to run 'make bison-local' before running
'make' again to rebuild with the generated parser files. The makefile requires
Bison >= 3.5, so I had to build this from source on a Debian 10.2 box (it's a
painless package to build, but needs an 'apt-get install m4', if it's not
already there).

This turns the alt-text into a string with spaces changed to underscores (so we
can extract it as a single word), and appends "Image" to the text so that we
can differentiate easily from normal 'href' links.

